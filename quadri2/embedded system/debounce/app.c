
#include "app.h"
#include "mcc_generated_files/pin_manager.h"

APP_LED_DATA appledData, appledData2;
APP_KEY_DATA apps2Data;


void APP_LED_Initialize(void)
{
    appledData.state = APP_LED_STATE_INIT;
    appledData.TimerCount = 0;
    appledData2.state = APP_LED_STATE_INIT;
    appledData2.TimerCount = 0;
}

void APP_S2_Initialize(void)
{
    apps2Data.state = APP_KEY_STATE_INIT;
    apps2Data.TimerDebounce = 0;
}


void APP_LED_Tasks(void)
{
    switch(appledData.state)
    {
        case APP_LED_STATE_INIT :
        {
            appledData.state = APP_LED_STATE_WAIT;
            break;
        }
        case APP_LED_STATE_WAIT : 
        {
            if(appledData.TimerCount >= 250)
            {
                appledData.state = APP_LED_STATE_BLINK_LED;
                appledData.TimerCount = 0;
            }
            break;
        }
        case APP_LED_STATE_BLINK_LED : 
        {
            D2_Toggle();
            appledData.state = APP_LED_STATE_WAIT;
            break;
        }
        
        case APP_LED_STATE_STOP :
        {
            break;
        }
        
        default :{
            appledData.state = APP_LED_STATE_INIT;
            break;
        }
    }
}

void APP_LED_Tasks2(void)
{
    switch(appledData2.state)
    {
        case APP_LED_STATE_INIT :
        {
            appledData2.state = APP_LED_STATE_WAIT;
            break;
        }
        case APP_LED_STATE_WAIT : 
        {
            if(appledData2.TimerCount >= 3270/2)
            {
                appledData2.state = APP_LED_STATE_BLINK_LED;
                appledData2.TimerCount = 0;
            }
            break;
        }
        case APP_LED_STATE_BLINK_LED : 
        {
            D3_Toggle();
            appledData2.state = APP_LED_STATE_WAIT;
            break;
        }
        
        case APP_LED_STATE_STOP :
        {
            break;
        }
        
        default :{
            appledData2.state = APP_LED_STATE_INIT;
            break;
        }
    }
}


void APP_S2_Tasks(void){
  switch(apps2Data.state)
    {
        case APP_KEY_STATE_INIT :
        {
            if(S2_GetValue()==1)
                apps2Data.state = APP_KEY_STATE_HIGH;
            else apps2Data.state = APP_KEY_STATE_LOW;
            break;
        }
      case APP_KEY_STATE_HIGH : 
      {
          if(S2_GetValue()==0)
          {
              apps2Data.state = APP_KEY_STATE_DEBOUNCE;
              apps2Data.TimerDebounce = 0;
          }
          break;
      }
      case APP_KEY_STATE_LOW : 
      {
          if(S2_GetValue()==1)
          {
              apps2Data.state = APP_KEY_STATE_DEBOUNCE;
              apps2Data.TimerDebounce = 0;
          }
          break;
      }
      case APP_KEY_STATE_DEBOUNCE : 
      {
          if(apps2Data.TimerDebounce >= 50)
          {
              if(S2_GetValue()==0)
                  apps2Data.state = APP_KEY_STATE_LOW;
              else 
              {
                  apps2Data.state = APP_KEY_STATE_HIGH;
                  if(appledData.state == APP_LED_STATE_STOP)
                      appledData.state = APP_LED_STATE_BLINK_LED;
                  else appledData.state = APP_LED_STATE_STOP;
                  
                  if(appledData2.state == APP_LED_STATE_STOP)
                      appledData2.state = APP_LED_STATE_BLINK_LED;
                  else appledData2.state = APP_LED_STATE_STOP;
              }
          }
          break;
      }
      default :
        {
            break;
        }
  }
}