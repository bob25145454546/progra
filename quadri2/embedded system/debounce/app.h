/* 
 * File:   app.h
 * Author: mar
 *
 * Created on 12 janvier 2020, 14:48
 */

#ifndef APP_H
#define	APP_H

#include <stdbool.h>
#include <stdint.h>

typedef enum
{
    APP_LED_STATE_INIT = 0,
    APP_LED_STATE_WAIT = 1,
    APP_LED_STATE_BLINK_LED = 2,  
    APP_LED_STATE_STOP = 3
} APP_LED_STATES;

typedef struct
{
    APP_LED_STATES state;
    int TimerCount;
}APP_LED_DATA;

typedef enum
{
    APP_KEY_STATE_INIT = 0,
    APP_KEY_STATE_HIGH = 1,
    APP_KEY_STATE_DEBOUNCE = 2,
    APP_KEY_STATE_LOW = 3
} APP_KEY_STATES;


typedef struct{
    APP_KEY_STATES state;
    int TimerDebounce; 
}APP_KEY_DATA;


extern APP_LED_DATA appledData, appledData2;
extern APP_KEY_DATA apps2Data;

void APP_LED_Initialize(void);
void APP_LED_Tasks(void);

void APP_S2_Initialize(void);
void APP_S2_Tasks(void);

#ifdef	__cplusplus
extern "C" {
#endif

#ifdef	__cplusplus
}
#endif

#endif	/* APP_H */

