/* 
 * File:   app.h
 * Author: mar
 *
 * Created on 12 janvier 2020, 14:48
 */

#ifndef APP_H
#define	APP_H

#include <stdbool.h>
#include <stdint.h>

typedef enum
{
    APP_LED_STATE_INIT = 0,
    APP_LED_STATE_WAIT = 1,
    APP_LED_STATE_BLINK_LED = 2,
    APP_LED_STATE_STOP = 3
} APP_LED_STATES;

typedef struct
{
    APP_LED_STATES state;
    int TimerCount;
}APP_LED_DATA;

extern APP_LED_DATA appledData;
extern APP_LED_DATA appledData2;

typedef enum
{
    APP_KEY_STATE_INIT = 0,
    APP_KEY_STATE_HIGH = 1,
    APP_KEY_STATE_DEBOUNCE = 2,
    APP_KEY_STATE_LOW = 3
} APP_KEY_STATES;

typedef struct{
    APP_KEY_STATES state;
    int TimerDebounce; 
}APP_KEY_DATA;

extern APP_KEY_DATA apps2Data;

typedef enum
{
    APP_ADC_STATE_INIT = 0,
    APP_ADC_STATE_CONVERT = 1,
    APP_ADC_STATE_WAIT = 2,
    APP_ADC_STATE_DONE = 3    
}APP_ADC_STATES;

typedef struct
{
    APP_ADC_STATES state;
    int adcCount;
    int adcValue;    
}APP_ADC_DATA;

extern APP_ADC_DATA appadcData;

void APP_LED_Initialize(void);
void APP_LED_Tasks(void);
void APP_LED_Tasks2(void);
void APP_S2_Initialize(void);
void APP_S2_Tasks(void);
void APP_ADC_Initialize(void);
void APP_ADC_Tasks(void);

#ifdef	__cplusplus
extern "C" {
#endif

#ifdef	__cplusplus
}
#endif

#endif	/* APP_H */

