
#include "app.h"
#include "mcc_generated_files/pin_manager.h"
#include "mcc_generated_files/adc.h"
#include "mcc_generated_files/pwm1.h"

APP_LED_DATA appledData, appledData2;
APP_KEY_DATA apps2Data;
APP_ADC_DATA appadcData;
int ADCValue = 0;

void APP_LED_Initialize(void) {
    appledData.state = APP_LED_STATE_INIT;
    appledData.TimerCount = 0;
    appledData2.state = APP_LED_STATE_INIT;
    appledData2.TimerCount = 0;
}

void APP_S2_Initialize(void) {
    apps2Data.state = APP_KEY_STATE_INIT;
    apps2Data.TimerDebounce = 0;
}

void APP_ADC_Initialize(void)
{
    appadcData.state = APP_ADC_STATE_INIT;
    appadcData.adcValue = 0;
    appadcData.adcCount = 0;
}

void APP_S2_Tasks(void) {
    switch (apps2Data.state) {
        case APP_KEY_STATE_INIT:
        {
            if (S2_GetValue() == 1)
                apps2Data.state = APP_KEY_STATE_HIGH;
            else apps2Data.state = APP_KEY_STATE_LOW;
            break;
        }
        case APP_KEY_STATE_HIGH:
        {
            if (S2_GetValue() == 0) {
                apps2Data.state = APP_KEY_STATE_DEBOUNCE;
                apps2Data.TimerDebounce = 0;
            }
            break;
        }
        case APP_KEY_STATE_LOW:
        {
            if (S2_GetValue() == 1) {
                apps2Data.state = APP_KEY_STATE_DEBOUNCE;
                apps2Data.TimerDebounce = 0;
            }
            break;
        }
        case APP_KEY_STATE_DEBOUNCE:
        {
            if (apps2Data.TimerDebounce >= 50) {
                if (S2_GetValue() == 0)
                    apps2Data.state = APP_KEY_STATE_LOW;
                else {
                    apps2Data.state = APP_KEY_STATE_HIGH;
                    if (appledData.state == APP_LED_STATE_STOP)
                        appledData.state = APP_LED_STATE_BLINK_LED;
                    else appledData.state = APP_LED_STATE_STOP;

                    if (appledData2.state == APP_LED_STATE_STOP)
                        appledData2.state = APP_LED_STATE_BLINK_LED;
                    else appledData2.state = APP_LED_STATE_STOP;
                }
            }
            break;
        }
        default:
        {
            break;
        }
    }
}

void APP_LED_Tasks(void) {
    switch (appledData.state) {
        case APP_LED_STATE_INIT:
        {
            appledData.state = APP_LED_STATE_WAIT;
            break;
        }
        case APP_LED_STATE_WAIT:
        {
            if (appledData.TimerCount >= 250) {
                appledData.state = APP_LED_STATE_BLINK_LED;
                appledData.TimerCount = 0;
            }
            break;
        }
        case APP_LED_STATE_BLINK_LED:
        {
            D2_Toggle();
            appledData.state = APP_LED_STATE_WAIT;
            break;
        }
        case APP_LED_STATE_STOP:
        {
            break;
        }
        default:
        {
            appledData.state = APP_LED_STATE_INIT;
            break;
        }
    }
}

void APP_LED_Tasks2(void) {
    switch (appledData2.state) {
        case APP_LED_STATE_INIT:
        {
            appledData2.state = APP_LED_STATE_WAIT;
            break;
        }
        case APP_LED_STATE_WAIT:
        {
            if (appledData2.TimerCount >= 327 / 2) {
                appledData2.state = APP_LED_STATE_BLINK_LED;
                appledData2.TimerCount = 0;
            }
            break;
        }
        case APP_LED_STATE_BLINK_LED:
        {
            D3_Toggle();
            appledData2.state = APP_LED_STATE_WAIT;
            break;
        }
        case APP_LED_STATE_STOP:
        {
            break;
        }
        default:
        {
            appledData2.state = APP_LED_STATE_INIT;
            break;
        }
    }
}

void APP_ADC_Tasks(void){
    switch(appadcData.state){
        case APP_ADC_STATE_INIT : 
        {
            ADC_SelectChannel(Pot); //connecter le channel au pot
            appadcData.adcCount = 0; // qui va jusque 16
            appadcData.adcValue = 0; // valeur de l'ADC
            appadcData.state = APP_ADC_STATE_CONVERT;
            break;
        }
        case APP_ADC_STATE_WAIT : 
        {
            if(ADC_IsConversionDone())
            {
                appadcData.adcValue = appadcData.adcValue + ADC_GetConversionResult();
               
                if(appadcData.adcCount >= 16)
                    appadcData.state = APP_ADC_STATE_DONE;
                else appadcData.state = APP_ADC_STATE_CONVERT;
            }
            break; 
        }
        case APP_ADC_STATE_DONE : 
        {
            appadcData.adcCount = 0;
            ADCValue = appadcData.adcValue >> 4;
            PWM1_LoadDutyValue(ADCValue);
            appadcData.adcValue = 0;
            appadcData.state = APP_ADC_STATE_CONVERT;
            break;
        }
        case APP_ADC_STATE_CONVERT : 
        {
            ADC_StartConversion();
            appadcData.adcCount++;
            appadcData.state = APP_ADC_STATE_WAIT;
            break;
        }
        default : 
        {
            break;
        }
    }
}