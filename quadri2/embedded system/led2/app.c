
#include "app.h"
#include "mcc_generated_files/pin_manager.h"

APP_LED_DATA appledData, appledData2;

void APP_LED_Initialize(void)
{
    appledData.state = APP_LED_STATE_INIT;
    appledData.TimerCount = 0;
    appledData2.state = APP_LED_STATE_INIT;
    appledData2.TimerCount = 0;
}

void APP_LED_Tasks(void)
{
    switch(appledData.state)
    {
        case APP_LED_STATE_INIT :
        {
            appledData.state = APP_LED_STATE_WAIT;
            break;
        }
        case APP_LED_STATE_WAIT : 
        {
            if(appledData.TimerCount >= 250)
            {
                appledData.state = APP_LED_STATE_BLINK_LED;
                appledData.TimerCount = 0;
            }
            break;
        }
        case APP_LED_STATE_BLINK_LED : 
        {
            D2_Toggle();
            appledData.state = APP_LED_STATE_WAIT;
            break;
        }
        default :{
            appledData.state = APP_LED_STATE_INIT;
            break;
        }
    }
}

void APP_LED_Tasks2(void)
{
    switch(appledData2.state)
    {
        case APP_LED_STATE_INIT :
        {
            appledData2.state = APP_LED_STATE_WAIT;
            break;
        }
        case APP_LED_STATE_WAIT : 
        {
            if(appledData2.TimerCount >= 327/2)
            {
                appledData2.state = APP_LED_STATE_BLINK_LED;
                appledData2.TimerCount = 0;
            }
            break;
        }
        case APP_LED_STATE_BLINK_LED : 
        {
            D3_Toggle();
            appledData2.state = APP_LED_STATE_WAIT;
            break;
        }
        default :{
            appledData2.state = APP_LED_STATE_INIT;
            break;
        }
    }
}