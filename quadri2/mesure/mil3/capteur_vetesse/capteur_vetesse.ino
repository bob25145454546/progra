#define WHEEL_RADIUS 0.015 // m
#define PI 3.1415926535897932384626433832795
#define PERIMETER 2*PI*WHEEL_RADIUS

int hall_effect_pin = 13;
int hall_effect_state = 0;
int hall_effect_detection_number = 0;
float Speed = 0;

void setup(){
  pinMode(hall_effect_pin, INPUT);
  Serial.begin(9600);
}

void loop(){
  
  hall_effect_state = digitalRead(hall_effect_pin);
  
  if(hall_effect_state == LOW){
    hall_effect_detection_number++;  
  }
  
  else{
    hall_effect_detection_number = 0;
  }
 
  if(hall_effect_detection_number > 0){
    Speed = hall_effect_detection_number*PERIMETER;
  }
  
  else if(hall_effect_detection_number == 0){
    Speed = 0;
  }
 
  Serial.println(Speed);
}
