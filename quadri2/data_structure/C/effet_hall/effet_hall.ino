#define WHEEL_RADIUS 0.015 // m
#define PI 3.1415926535897932384626433832795
#define PERIMETER 2*PI*WHEEL_RADIUS
#define NUMBEROFTOOTH 60

struct hallSensor{
  int pin = 13;
  bool currentState = LOW;
  int oldState = ~currentState;
  int detectionNumber = 0;
}sensor;

int currentNumberOfTooth = 0;
float speedOfWhell = 0;
unsigned long referenceTime, currentTime, elapsedTime;


void setup(){
  pinMode(sensor.pin, INPUT);
  Serial.begin(9600);
  referenceTime = micros();
  currentTime = micros();
}

void loop(){
  
  sensor.currentState = digitalRead(sensor.pin);
  currentTime = millis();
  elapsedTime = currentTime - referenceTime;
  
  if(sensor.currentState == LOW && (sensor.currentState != sensor.oldState)){
    sensor.detectionNumber++; 
    currentNumberOfTooth++; 
    sensor.oldState = LOW;
  }
  
  else if (sensor.currentState == HIGH){
    sensor.oldState = HIGH;
  }
 
  if(elapsedTime >= 1000){
    //Speed = sensor.detectionNumber*PERIMETER;
    speedOfWhell = currentNumberOfTooth/NUMBEROFTOOTH;
    currentNumberOfTooth = 0;
    referenceTime = micros();
  }
 
  Serial.println(speedOfWhell);
}
