from cryptography.exceptions import InvalidSignature
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.backends import default_backend

# public_exponent ==> l'exposant public de la clé générer, par défaut conseillé à 65537 (lié à la formule math)
# key_size ==> taille de la clé en bit, recommandé au minimum à 2048
# backend ==> ??
privateKey = rsa.generate_private_key(public_exponent=65537, key_size=2048, backend=default_backend())
publicKey = privateKey.public_key()

otherPrivateKey = rsa.generate_private_key(public_exponent=65537, key_size=4096, backend=default_backend())

while 1:
    sha256 = hashes.SHA256()
    print('Entrer quelque chose à crypter :')
    word = input()

    if str(word) == '0':
        break

    wordBit = bytes(word, 'utf-8')  # transforme le mot donnée en type bytes

    # PSS ==> recommandé
    # signe le message pour qu'on puisse vérifier qu'il provient bien de celui qui à ajouté la signature avec sa clé privée
    #wordEncryptedAndSigned
    # padding ==> rembourrage de données aléatoire pour éviter des soucis de décryptage de clé privée +o-
    wordEncrypted = publicKey.encrypt(wordBit, padding.OAEP(mgf=padding.MGF1(algorithm=sha256),
                                                            algorithm=sha256,
                                                            label=None))

    wordEncryptedSigned = privateKey.sign(wordEncrypted, padding.PSS(mgf=padding.MGF1(sha256),
                                                                     salt_length=padding.PSS.MAX_LENGTH),
                                                                     sha256)


    wordEncryptedSigned = otherPrivateKey.sign(wordEncrypted, padding.PSS(mgf=padding.MGF1(sha256),
                                                                     salt_length=padding.PSS.MAX_LENGTH),
                                                                     sha256)



    print('  word Signed and Encrypted : ', wordEncryptedSigned, '\n')
    # mgf/salt_length ==> recommandé de pas y toucher
    try:
        # est censé levé une exception si faux, donc normalement juste
        publicCheck = publicKey.verify(wordEncryptedSigned, wordEncrypted, padding.PSS(mgf = padding.MGF1(sha256),
                                                                                       salt_length = padding.PSS.MAX_LENGTH),
                                                                                        sha256)

        wordSignedDecrypted = privateKey.decrypt(wordEncrypted, padding.OAEP(mgf=padding.MGF1(algorithm=sha256),
                                                                             algorithm=sha256,
                                                                             label=None))
        print('  the private and public key matche \n')
        wordOrignal = wordSignedDecrypted.decode('ascii')
        print('  original word : ', wordOrignal, '\n')

    except InvalidSignature:
        print('  the message encrypted with public key has been signed with another private key \n')