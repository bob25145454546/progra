import hashlib


while 1:
    hashFunc = hashlib.sha256()  # permet de construire l'objet avec le bon algo

    print('Entrer quelque chose à <hasher> :')
    word = input()

    if str(word) == '0':
        break

    wordBit = bytes(word, 'utf-8') # transforme le mot donnée en type bytes

    hashFunc.update(wordBit) # ajout le mot byte
    result = hashFunc.digest() # effectue la conversion

    print(result,'\n')