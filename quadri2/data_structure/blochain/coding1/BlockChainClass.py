# hello-blockchain.py
# Author: Sébastien Combéfis
# Version: April 23, 2020

import hashlib
import time

class Block:
    def __init__(self, index, timestamp, prevHash, data):
        self.index = index
        self.timestamp = timestamp
        self.prevHash = prevHash
        self.data = data
        self.hash = self.__computeHash__()
        pass

    def __computeHash__(self):
        hashFunc = hashlib.sha256()
        wordBit = bytes(self.data, 'utf-8')
        hashFunc.update(wordBit)
        return hashFunc.digest()

    def __str__(self):
        pass


class GenesisBlock(Block):
    def __init__(self, index, timestamp, data):
        super().__init__(index, timestamp, None, data)
        self.hash = "0000"


class BlockChain:
    def __init__(self, name):
        self.name = name
        self.blocks = []

    def append(self, data):
        '''Add a new piece of data in this blockchain.'''
        if(self.__len__() == 0):
            self.blocks.append(GenesisBlock(0, time.time(), data))
        else:
            self.blocks.append(Block(self.__len__(), time.time(), self.blocks[self.__len__() - 1].hash, data))
            if(self.__len__() == 2):
                #self.blocks.append(Block(self.__len__(), time.time(), "0", data))
                x=5
        pass

    def isValid(self):
        '''Checks whether this blockchain is valid.'''
        isValid = True
        for block in self.blocks:
            if(block.index == 0 and self.__len__() > 1):
                if(block.hash != self.blocks[block.index+1].prevHash):
                    isValid = False

            else:
                if(block.prevHash != self.blocks[block.index-1].hash):
                    isValid = False
        return isValid

    def chain(self):
        return self.blocks

    def __len__(self):
        return len(self.blocks)

    def __getitem__(self, i):
        return self.blocks[i]

    #def __str__(self):
        #pass


if __name__ == '__main__':
    blockchain = BlockChain('CombéCoin')
    print(blockchain.name)

    blockchain.append("aaaaa")
    blockchain.append("bbbbb")
    blockchain.append("ccc")
    blockchain.append("ddd")
    
    print(blockchain. isValid())

    for block in blockchain.chain():
        print(block.data)
