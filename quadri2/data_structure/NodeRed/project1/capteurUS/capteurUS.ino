struct USsensor{
  unsigned char id;
  unsigned char triggerPin;
  unsigned char echoPin;
  double lastDistance;
}usSensor1, usSensor2, usSensor3;

double distanceUSsensor1, distanceUSsensor2, distanceUSsensor3;
double wait;
unsigned int numberOfMesure;

void setup() {
  Serial.begin(9600);
  
  usSensorInitialize(&usSensor1, 2, 3, 1);
  //usSensorInitialize(&usSensor2, 4, 5, 2);
  //usSensorInitialize(&usSensor3, 6, 7, 3);

  wait = 0.5; // nombre de seconde à atendre entre chaque prise de mesure
  
  distanceUSsensor1 = 0;
  distanceUSsensor2 = 0;
  distanceUSsensor3 = 0;

  numberOfMesure = 10;
}

 
void loop() { 
  
  displayDistance(&usSensor1);
  //displayDistanceResult(&usSensor2);
  //displayDistanceResult(&usSensor3);
  
  Serial.print(" , ");
  
  displayAverageDistance(&usSensor1, numberOfMesure);
  //displayAreageDistanceResult(&usSensor2, numberOfMesure);
  //displayAreageDistanceResult(&usSensor3, numberOfMesure);
  
  delay(wait*1000);
}

void usSensorInitialize(USsensor *usSensor, unsigned char trig, unsigned char echo, unsigned char id){
  usSensor->triggerPin = trig;
  usSensor->echoPin = echo;
  usSensor->id = id;
  pinMode(trig, OUTPUT);
  pinMode(echo, INPUT);
}

double averageDistance(USsensor *usSensor, unsigned int numberOfMesures){
  unsigned int incre = 0;
  double average = 0;
  
  while(incre < numberOfMesures){
    average += distanceComputation(usSensor);
    incre++;
  }

  return average/numberOfMesures;
}

double distanceComputation(USsensor *usSensor){
  double times, distanceInCM;

  digitalWrite(usSensor->triggerPin, HIGH);
  delayMicroseconds(10);                   
  digitalWrite(usSensor->triggerPin, LOW);

  times = pulseIn(usSensor->echoPin, HIGH);  // retourne des micro secondes
  distanceInCM = (times*340/2)/pow(10,4);   // pow(nombre, exposant) transforme en cm

  if((distanceInCM > 5*usSensor->lastDistance) && (usSensor->lastDistance != 0)){
    distanceInCM = usSensor->lastDistance;
  }
  
  usSensor->lastDistance = distanceInCM;
  return distanceInCM;
}

void displayDistance(USsensor *usSensor){
  double distanceUSsensor = 0;
  distanceUSsensor = distanceComputation(usSensor);
  Serial.print(distanceUSsensor);
  /*
  Serial.print(distanceUSsensor);
  Serial.print(" cm, ");
  Serial.print("numero du capteur");
  Serial.print(" : ");
  Serial.println(usSensor->id);
  */
}

void displayAverageDistance(USsensor *usSensor, unsigned int echantillons){
  double distanceUSsensor = 0;
  distanceUSsensor = averageDistance(usSensor, echantillons);
  Serial.print("average : ");
  Serial.println(distanceUSsensor);
  /*
  Serial.print(distanceUSsensor);
  Serial.print(" cm MOYEN, ");
  Serial.print("numero du capteur");
  Serial.print(" : ");
  Serial.println(usSensor->id);
  */
}
