﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace library
{
    public interface Computer
    {
        string name
        {
            get;
        }

        double compute(params double[] values);
    }
}
