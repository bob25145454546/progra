﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace library
{
    public class Adder : Computer
    {
        public string name
        {
            get { return "Adder"; }
        }

        public double compute(params double[] values)
        {
            double result = 0;
            Array.ForEach(values, value => result += value);
            return result;
        }
    }
}
