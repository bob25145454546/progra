﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace labo4
{
    public static class PrintData
    {
        public static void printData(Object obj)
        {
            Type objectType = obj.GetType();

            //Récupération des informations concernant les propriétés de l'objet
            PropertyInfo[] properties = objectType.GetProperties();
            MethodInfo[] methods = objectType.GetMethods();
            //Révèle le type sous-jacent
            Console.WriteLine(obj.GetType().Name);

            foreach (var property in properties)
            {
                string propertyData = String.Format("Property : {0} - Type : {1}", property.Name, property.PropertyType.Name);
                Console.WriteLine(propertyData);
            }
            foreach (var method in methods)
            {
                string methodData = String.Format("Method : {0} - Return type : {1}", method.Name, method.ReturnType);
                Console.WriteLine(methodData);
            }
        }
    }
}
