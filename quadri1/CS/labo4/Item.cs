﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labo4
{
    public class Item
    {
        private static int _nextid = 0;

        public readonly int id;
        private readonly string _name;
        private readonly int _price;

        public Item(string name, int price)
        {
            this.id = ++_nextid;
            this._name = name;
            this._price = price;
        }

        public string name
        {
            get { return _name; }
        }

        public int price
        {
            get { return _price; }
        }
    }
}
