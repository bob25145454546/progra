﻿namespace fenetre
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.compute = new System.Windows.Forms.Button();
            this.equationInput = new System.Windows.Forms.TextBox();
            this.computeOutput = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // compute
            // 
            this.compute.Location = new System.Drawing.Point(333, 279);
            this.compute.Name = "compute";
            this.compute.Size = new System.Drawing.Size(146, 43);
            this.compute.TabIndex = 0;
            this.compute.Text = "compute";
            this.compute.UseVisualStyleBackColor = true;
            this.compute.Click += new System.EventHandler(this.compute_Click);
            // 
            // equationInput
            // 
            this.equationInput.Location = new System.Drawing.Point(102, 302);
            this.equationInput.Name = "equationInput";
            this.equationInput.Size = new System.Drawing.Size(202, 20);
            this.equationInput.TabIndex = 3;
            this.equationInput.TextChanged += new System.EventHandler(this.EquationInput_TextChanged);
            // 
            // computeOutput
            // 
            this.computeOutput.Location = new System.Drawing.Point(102, 255);
            this.computeOutput.Name = "computeOutput";
            this.computeOutput.ReadOnly = true;
            this.computeOutput.Size = new System.Drawing.Size(202, 20);
            this.computeOutput.TabIndex = 4;
            this.computeOutput.TextChanged += new System.EventHandler(this.ComputeOutput_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(571, 450);
            this.Controls.Add(this.computeOutput);
            this.Controls.Add(this.equationInput);
            this.Controls.Add(this.compute);
            this.Name = "Form1";
            this.Text = "result";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button compute;
        private System.Windows.Forms.TextBox equationInput;
        private System.Windows.Forms.TextBox computeOutput;
    }
}

