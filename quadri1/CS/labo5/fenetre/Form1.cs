﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace fenetre
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void compute_Click(object sender, EventArgs e)
        {
            double equationResult = Compute.computeEquation(this.equationInput.Text);
            this.computeOutput.Text = equationResult.ToString();
        }
       
        private void EquationInput_TextChanged(object sender, EventArgs e)
        {

        }

        private void ComputeOutput_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
