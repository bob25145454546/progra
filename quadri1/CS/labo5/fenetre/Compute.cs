﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fenetre
{
    public static class Compute
    {
        public static double computeEquation(string equation)
        {
            double result = 0.0;
            char signe = ' ';

            foreach(char s in equation)
            {
                if (s == '+' || s == '-' || s == '*' || s == '/')
                {
                    signe = s;
                } 
            }

            string[] equaStrArray = equation.Split(new string[] { "+","-","*","/" }, StringSplitOptions.RemoveEmptyEntries);
            
            if(signe == '+')
            {
                result = Convert.ToDouble(equaStrArray[0]) + Convert.ToDouble(equaStrArray[1]);
            }
            else if (signe == '-')
            {
                result = Convert.ToDouble(equaStrArray[0]) - Convert.ToDouble(equaStrArray[1]);
            }
            else if (signe == '*')
            {
                result = Convert.ToDouble(equaStrArray[0]) * Convert.ToDouble(equaStrArray[1]);
            }
            else if (signe == '/')
            {
                result = Convert.ToDouble(equaStrArray[0]) / Convert.ToDouble(equaStrArray[1]);
            }

            return result;
        }     
    }
}
