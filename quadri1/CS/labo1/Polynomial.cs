﻿using System;
using System.Collections.Generic;


public class Polynomial
{
    private List<Double> list = new List<Double>();
    private int varDegre = 0;
    private string varChaine = "";

    public Polynomial(List<Double> list)
    {
        this.list = list;
        varDegre = list.Count - 1;
        varChaine = chaine();
    }

    public double evaluate(int x)
    {
        double sum = 0.0;

        foreach (double e in list)
        {
            sum += Math.Pow(x, varDegre) * e;
            varDegre -= 1;
        }
        return sum;
    }

    public override string ToString()
    {
        return varChaine;
    }

    public bool Equals(object x)
    {
        if (this.GetType() == x.GetType()) return true;
        else return false;
    }

    private string chaine()
    {
        string poly = "";
        int degre = varDegre;

        foreach (double e in list)
        {
            poly += e + "x^" + degre;
            if (degre > 0) { poly += " + "; }
            degre -= 1;
        }
        return poly;
    }

    public int degre() { return varDegre; }


}
