﻿using System;
using System.Timers;


public class CountDown
{
    private static int seconde;
    private static Timer timer;

    public CountDown(int s)
    {
        seconde = s;
    }

    public int RemainingTime()
    {
        return seconde;
    }

    public void start()
    {
        timer = new Timer(1000);
        if (seconde > 0)
            timer.Elapsed += OnTimedEvent;
        timer.AutoReset = true;
        timer.Enabled = true;
    }

    private static void OnTimedEvent(Object source, ElapsedEventArgs e)
    {
        String txt = "" + e.SignalTime;
        Console.WriteLine("e : " + txt.Remove(0, 15));
        seconde--;
    }

    public void stop()
    {
        timer.Stop();
    }
}
