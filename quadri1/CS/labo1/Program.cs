﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;



namespace Labo1
{
    class Program
    {
        static String n = "\n";
        private static Timer aTimer;
        static void Main(string[] args)
        {
            Console.Write(n);

            CountDown cd = new CountDown(600);

            cd.start();
            Console.Write(cd.RemainingTime());
            Console.ReadLine();

            //exo1_2();
            //exo3();

            //Console.Write("2 : " + cd.RemainingTime());

            Console.Write(n);
        }


        private static String Entre(String a, String b, String c)
        {
            double aa = double.Parse(a);
            double bb = double.Parse(b);
            double cc = double.Parse(c);

            double delta = (Math.Pow(bb, 2)) - (4 * aa * cc);
            Console.Write("discriminant : " + delta + n);

            if (delta < 0)
            {
                double x = 0;
                Console.Write("pas de racine reel double " + n);

                return x.ToString();
            }

            else if (delta == 0)
            {
                double x = -bb / (2 * aa);
                Console.Write("une racine reel double : " + x + n);

                return x.ToString();
            }

            else
            {
                double x1 = (-bb - Math.Sqrt(delta) / (2 * aa));
                double x2 = (-bb + Math.Sqrt(delta) / (2 * aa));
                String x12 = x1.ToString() + ";" + x2.ToString();
                Console.Write("deux racines reel distincte : " + x1 + " et " + x2 + n + n);

                return x12;
            }
        }

        private static bool TestEntre(String number)
        {
            try
            {
                double test = double.Parse(number) / 2.0;
                return true;
            }
            catch (Exception ex)
            {
                Console.Write("Mauvais nombre \n");
                return false;
            }
        }

        private static String ReadIn(String number)
        {
            String a = "";
            while (true)
            {
                Console.Write("Enter coef {0} : ", number);
                a = Console.ReadLine();
                if (TestEntre(a)) { break; }
            }
            return a;
        }

        private static void exo1_2()
        {
            String a = "";
            String b = "";
            String c = "";

            bool flaga = false;
            bool flagb = false;
            bool flagc = false;

            a = ReadIn("a");
            b = ReadIn("b");
            c = ReadIn("c");

            Entre(a, b, c);
        }

        private static void exo3()
        {
            Polynomial po = new Polynomial(new List<Double>() { 4.0, 3.0, 2.0, 5.0, 8.0 });
            Polynomial poo = new Polynomial(new List<Double>() { 4.0, 3.0, 2.0, 5.0, 8.0 });
            Console.Write("" + po.degre());
            Console.Write(n);
            Console.Write(po.evaluate(3));
            Console.Write(n);
            Console.Write(po);
            Console.Write(n);
            Console.Write(po == poo);
            Console.Write(n);
            Console.Write(po.Equals(poo));
        }
    }
}
