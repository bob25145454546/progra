﻿using System;
using System.Collections.Generic;
using System.Text;

namespace testExam
{
    public class Perso
    {
        protected string _nom;
        protected string _pren;

        public Perso(string nom, string pren)
        {
            Console.WriteLine("Perso construct");
            _nom = nom;
            _pren = pren;
        }

        public virtual void print()
        {
            Console.WriteLine(" " + _nom + ' ' + _pren);
        }

        public virtual void tes()
        {
            Console.WriteLine("Perso test");
        }

    }
}
