﻿using System;
using System.Collections.Generic;
using System.Text;

namespace testExam
{
    public class A
    {
        protected int a = 21;

        public virtual int secret (int a)
        {
            Console.WriteLine("virtual");

            return a;
        }

        public int secret()
        {
            Console.WriteLine("secret base");
            return this.secret(42);
        }
    }
}
