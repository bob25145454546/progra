﻿using System;
using System.Collections.Generic;
using System.Text;

namespace testExam
{
    class Moi : Perso
    {

        public Moi(string nom, string pren) : base(nom, pren)
        {
            Console.WriteLine("moi construct");
            _nom = nom + " -> override";
            _pren = pren + " -> override";
        }

        public new void print()
        {
            Console.WriteLine("over");
            base.print();
        }

        public new void tes(string x)
        {
            Console.WriteLine("moi test" + x);
        }
    }
}
