﻿using System;
using System.Collections.Generic;


namespace labo3
{
    public static class DyckWord
    {

        public static bool checDyckWord(string word)
        {
            bool isDyck = false;
            Stack<char> stack = new Stack<char>();

            if (word != "")
            {
                foreach (char parenthese in word)
                {
                    if (parenthese == '(')
                    {
                        stack.Push(parenthese);
                    }
                    else if (stack.Count != 0)
                    {
                        stack.Pop();
                    }
                    else
                    {
                        stack.Push(parenthese);
                    }
                }
         
                if (stack.Count == 0)
                {
                    isDyck = true;
                }
            }
            return isDyck;
        }

                                                                      //key     value
        public static bool checDyckWordWithList(string word, Dictionary<char, char> alphabet)
        {
            bool isDyck = false;
            bool flagStrContainDick = false;
            bool flagCharIsAKey = false;
            bool flagCharIsAValue = false;
            Stack<char> stack = new Stack<char>();

            if (alphabet.Count != 0 && word != "")
            {
                foreach (char charInStr in word)
                {
                    flagCharIsAKey = alphabet.ContainsKey(charInStr);
                    flagCharIsAValue = alphabet.ContainsValue(charInStr);

                    if (flagCharIsAKey)
                    {
                        flagStrContainDick = true;
                        stack.Push(charInStr);
                    }
                    else if (stack.Count != 0 && flagCharIsAValue)
                    {
                        flagStrContainDick = true;
                        stack.Pop();
                    }
                    else if (flagCharIsAValue)
                    {
                        stack.Push(charInStr);
                    }
                }
                if (stack.Count == 0 && flagStrContainDick)
                {
                    isDyck = true;
                }
            }
            return isDyck;
        }
    }
}
