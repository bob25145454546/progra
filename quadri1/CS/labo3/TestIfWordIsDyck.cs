﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace labo3
{
    [TestFixture()]
    public class TestIfWordIsDyck
    {
        [SetUp()]
        public void Init()
        {
        }

        [Test()]
        public void TestDyckWork()
        {
            Assert.AreEqual(true, DyckWord.checDyckWord("()()"));
            Assert.AreEqual(true, DyckWord.checDyckWord("()"));
            Assert.AreEqual(true, DyckWord.checDyckWord("((()))"));

            Assert.AreEqual(false, DyckWord.checDyckWord("("));
            Assert.AreEqual(false, DyckWord.checDyckWord("()()("));
            Assert.AreEqual(false, DyckWord.checDyckWord("())"));
            Assert.AreEqual(false, DyckWord.checDyckWord("((())"));
            Assert.AreEqual(false, DyckWord.checDyckWord(")))"));
            Assert.AreEqual(false, DyckWord.checDyckWord("((("));
            Assert.AreEqual(false, DyckWord.checDyckWord(""));
        }

        [Test()]
        public void checDyckWordWithList()
        {
            //string bonneEntrer = "f(aa,[21,2,3])";// est valide
            Dictionary<char, char> dicOne = new Dictionary<char, char>()
                                                {
                                                { '(', ')' },
                                                { '{', '}'},
                                                { '[', ']'}
                                                };

       

            Assert.AreEqual(true, DyckWord.checDyckWordWithList("()",dicOne));
            Assert.AreEqual(true, DyckWord.checDyckWordWithList("[]",dicOne));
            Assert.AreEqual(true, DyckWord.checDyckWordWithList("{}",dicOne));
            Assert.AreEqual(true, DyckWord.checDyckWordWithList("[ergerg]",dicOne));
            Assert.AreEqual(true, DyckWord.checDyckWordWithList("[er(ge{r}g)]",dicOne));
            Assert.AreEqual(true, DyckWord.checDyckWordWithList("([ergerg])",dicOne));
            Assert.AreEqual(true, DyckWord.checDyckWordWithList("({er[gerg}])",dicOne));          

            Assert.AreEqual(false, DyckWord.checDyckWordWithList("([ergerg]()",dicOne));
            Assert.AreEqual(false, DyckWord.checDyckWordWithList("([ergerg](",dicOne));
            Assert.AreEqual(false, DyckWord.checDyckWordWithList("",dicOne));
            Assert.AreEqual(false, DyckWord.checDyckWordWithList(" ",dicOne));
            Assert.AreEqual(false, DyckWord.checDyckWordWithList("a",dicOne));


        }

        [Test()]
        public void checDyckWordWithListTwo()
        {
            Dictionary<char, char> dicTwo = new Dictionary<char, char>()
                                                {
                                                { 'r', 'g' }
                                                };

            Assert.AreEqual(false, DyckWord.checDyckWordWithList(")(", dicTwo));
            Assert.AreEqual(true, DyckWord.checDyckWordWithList(")ru$][tmlg](", dicTwo));
        }
    }
}
