﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    public class Professeur : Personne
    {
        private int _salair;
        public int salair
        {
            get
            {
                return this._salair;
            }
            set
            {
                this._salair = value;
            }
        }

        public Professeur(string nom, string prenom) : base(nom, prenom)
        {
            this._salair = 1800;
            ListObjectToSav.addObjToList(this);
        }

    }
}
