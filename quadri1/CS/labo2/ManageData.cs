﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace lab2
{
    public class ManageData
    {
        private string _pathDossier;
        private string _extension;
        public string pathDossier
        {
            get
            {
                return this._pathDossier;
            }
            set
            {
                this._pathDossier = value;
            }
        } 
        public string extension
        {
            get
            {
                return this._extension;
            }
            set
            {
                this._extension = value;
            }
        } 

        public ManageData(string extension)
        {
            this._extension = extension;
            this._pathDossier = Directory.GetParent(Environment.CurrentDirectory).Parent.FullName;
            this._pathDossier += "\\donnee";
            createFileAndFolder();
        }

        public ManageData()
        {
            this._extension = ".txt";
            this._pathDossier = Directory.GetParent(Environment.CurrentDirectory).Parent.FullName;
            this._pathDossier += "\\donnee";
            createFileAndFolder();
        }

        private void createFileAndFolder()
        {
            List<string> listObj = new List<string>() { "Etudiant", "Professeur", "Activiter", "Evaluations" };

            if (Directory.Exists(_pathDossier))
            {
                //Console.WriteLine(this._pathDossier+" existe déja");
            }
            else
            {
                DirectoryInfo di = Directory.CreateDirectory(_pathDossier);
            }

            foreach (string nomFichier in listObj)
            {
                string fichierAouvir = this._pathDossier + "\\" + nomFichier + this._extension;
                File.WriteAllText(fichierAouvir, "");
            }
        }
        
        public void takeDataToSav(List<Object> listObj)
        {
            string json = "";
            string typeObj = "";

            foreach (Object obj in listObj)
            {
                try
                {
                    typeObj = obj.ToString().Replace("lab2.", "");
                }
                catch
                {
                    typeObj = "inconnu";
                }

                json = JsonConvert.SerializeObject(obj);
                savData(json, typeObj);
            }
        }

        private void savData(string objectData, string nomFichier)
        {
            string completPath = this._pathDossier + "\\" + nomFichier + this._extension;
            using (StreamWriter file =
            new StreamWriter(completPath, true))
            {
                file.WriteLine(objectData);
            }
        }

        public List<Object> readAllData()
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(this.pathDossier);
            FileInfo[] listFiles = directoryInfo.GetFiles("*" + this._extension);
            string[] listLinesInFiles;
            List<Object> listObjInFiles = new List<object>();
            Object obj;

            foreach (FileInfo file in listFiles)
            {
                listLinesInFiles = File.ReadAllLines(this._pathDossier + "\\" + file);
                foreach (string line in listLinesInFiles)
                {
                    obj = JsonConvert.DeserializeObject<Object>(line);
                    listObjInFiles.Add(obj);
                }
            }
            return listObjInFiles;
        }
        
        public void resetFiles ()
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(this.pathDossier);
            FileInfo[] listFiles = directoryInfo.GetFiles("*"+ this._extension); 
            string completPath = "";

            foreach (FileInfo file in listFiles)
            {
                completPath = this._pathDossier + "\\" + file;
                File.WriteAllText(completPath, string.Empty);
            }
        }
        public void resetFiles (string fileName)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(this.pathDossier);
            FileInfo[] file = directoryInfo.GetFiles(fileName + this._extension);
            string completPath = this._pathDossier + "\\" + file[0];
            File.WriteAllText(completPath, string.Empty);
        }
    }
}
