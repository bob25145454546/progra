﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    class Appreciation : Evaluations
    {
        protected string _appreciation;
        public string appreciation
        {
            get
            {
                if (this._note >= 20)
                {
                    this._appreciation = "N";
                }

                else if (this._note >= 16)
                {
                    this._appreciation = "C";
                } 
                
                else if (this._note >= 12)
                {
                    this._appreciation = "B";
                }
                
                else if (this._note >= 8)
                {
                    this._appreciation = "TB";
                } 
                
                else if (this._note >= 4)
                {
                    this._appreciation = "X";
                }
                
                return this._appreciation;
            }
            set
            {
                this._appreciation = value;
            }
        }

        public Appreciation (Activiter activiter, int note) : base(activiter, note)
        {
        }

    }
}
