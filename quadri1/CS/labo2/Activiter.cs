﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    public class Activiter
    {
        protected Professeur _prof;
        public Professeur prof
        {
            get
            {
                return this._prof;
            }
        }
        protected string _name;
        public string name
        {
            get
            {
                return this._name;
            }
            set
            {
                this._name = value;
            }
        }
        protected int _ects;
        public int ects
        {
            get
            {
                return this._ects;
            }
        }
        protected string _code;
        public string code
        {
            get
            {
                return this._code;
            }
        }

        public Activiter(Professeur prof, string name, int ects, string code)
        {
            this._prof = prof;
            this._name = name;
            this._ects = ects;
            this._code = code;
            ListObjectToSav.addObjToList(this);
        }

    }
}
