﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    public class Evaluations
    {
        protected Activiter _activiter;
        public Activiter activiter
        {
            get
            {
                return this._activiter;
            }
        }

        protected float _note;
        public float note
        {
            get
            {
                return this._note;
            }
        }

        public Evaluations(Activiter activiter, int note)
        {
            this._activiter = activiter;
            this._note = note;
            ListObjectToSav.addObjToList(this);
        }

    }
}
