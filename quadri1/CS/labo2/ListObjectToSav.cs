﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    public static class ListObjectToSav
    {
        private static List<Object> _listObjets = new List<Object>();
        public static List<Object> listObjets
        {
            get
            {
                return _listObjets;
            }
        }

        public static void addObjToList(Object obj)
        {
            _listObjets.Add(obj);
        }
    }
}