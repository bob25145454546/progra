﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    class Cote : Evaluations
    {
        public float getSetNote
        {
            get
            {
                return this._note;
            }
            set
            {
                this._note = value;
            }
        }

        public Cote(Activiter activiter, int note) : base(activiter, note)
        {
        }
    }
}
