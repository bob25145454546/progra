﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    public class Personne
    {
        private string _nom;
        public string nom
        {
            get
            {
                return this._nom;
            }
            set
            {
                this._nom = value;
            }
        }

        private string _prenom;
        public string prenom
        {
            get
            {
                return this._prenom;
            }
            set
            {
                this._prenom = value;
            }
        }

        protected Personne(string nom, string prenom)
        {
            this._nom = nom;
            this._prenom = prenom;
        }
    }
}
