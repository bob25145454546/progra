﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    public class Etudiant : Personne
    {
        private List<Evaluations> _listEvaluations;
        public List<Evaluations> listEvaluations
        {
            get
            {
                return this._listEvaluations;
            }
        }
        private float _average;
        public float average
        {
            get
            {
                this._average = computeAverage();
                return this._average;
            }
 
        }

        public Etudiant(string nom, string prenom) : base(nom, prenom)
        {
            this._listEvaluations = new List<Evaluations>();
            this._average = 0;
            ListObjectToSav.addObjToList(this);
        }
             
        public void addEvaluations(Evaluations eval)
        {
            this._listEvaluations.Add(eval);
        }

        private float computeAverage()
        {
            float average = 0;
            int incre = 0;

            foreach (Evaluations eval in listEvaluations)
            {
                average += eval.note;
                incre++;
            }
            return average / incre;
        }
    }
}
