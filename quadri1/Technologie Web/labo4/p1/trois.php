<?php 
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    
    $_SESSION['prix'] = 0;
    $_SESSION['choixCours'] = array ('','','','');
    $_SESSION['prixCours'] = array ('aucun',0);
    $incre = 0;

    if(isset($_POST['validerChoix'])){
        if(!empty($_POST['cours'])){
            foreach($_POST['cours'] as $selected){
                $_SESSION['prixCours'] = explode(" ",$selected);
                $_SESSION['coursSelect'][$incre] = $_SESSION['prixCours'][0];
                $_SESSION['prix'] += $_SESSION['prixCours'][1];
                $incre++;
            }
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>dernière page</title>
        <link rel="stylesheet" href="info.css">
    </head>
    <body>
        <h1>Récapitulatif</h1>
        <div> 
            Nom : <?php echo $_SESSION['nom'] ?> <br/>
            Prénom : <?php echo $_SESSION['prenom'] ?> <br/><br/>
        </div>
        <div> 
            Vous avez choisi les formation : <br/><br/> 
            <?php 
                $incre = 0;
                foreach($_POST['cours'] as $selected){
                    echo "     -  ".$_SESSION['coursSelect'][$incre]."<br/>";
                    $incre++;
                } 
            ?> <br/>
        </div>
        <div> 
            Pour un total de <?php echo $_SESSION['prix'] ?>€
        </div>

    </body>
</html>