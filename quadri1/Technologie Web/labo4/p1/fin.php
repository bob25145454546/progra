<?php 
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
        session_destroy();
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Destruction session</title>
        <link rel="stylesheet" href="info.css">
    </head>
    <body>
        <h1>fin de la session</h1>
        <button class="b1" onClick="window.location.href='./un.php'" type="button">Page principal</button>
    </body>
</html>