<?php 
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    
    if( (isset($_POST['nom'])) And (isset($_POST['prenom'])) ){
        $_SESSION['nom'] = $_POST['nom'];
        $_SESSION['prenom'] = $_POST['prenom'];
    }  
    $_SESSION['flag'] = 1;

    $_SESSION['php'] = 0;
    $_SESSION['xml'] = 0;
    $_SESSION['java'] = 0;
    $_SESSION['cpp'] = 0;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>seconde page</title>
        <link rel="stylesheet" href="info.css">
    </head>
    <body>
        <h1>Sélection des formats</h1>
        <form method="post" action="./trois.php" > 
            <label>PHP 250€</label> : <input type="checkbox" name="cours[]" value="php 250" /><br/><br/>
            <label>XML 350€</label> : <input type="checkbox" name="cours[]" value="xml 350" /><br/><br/>
            <label>JAVA 450€</label> : <input type="checkbox" name="cours[]" value="java 450" /><br/><br/>
            <label>C++ 550€</label> : <input type="checkbox" name="cours[]" value="cpp 550" /><br/><br/><br/>
            
            <input type="submit" name="validerChoix" value="Page suivante"/>
            <button class="b1" onClick="window.location.href='./un.php'" type="button" >Page précédente</button>
            <button class="b1" onClick="window.location.href='./fin.php'" type="button">Annuler</button>
        </form> 
    </body>
</html>