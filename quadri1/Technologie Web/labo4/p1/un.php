<?php 
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    if(!isset($_SESSION['flag'])){
        $_SESSION['prenom'] = '';
        $_SESSION['nom'] = '';
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>page principal</title>
        <link rel="stylesheet" href="info.css">
    </head>
    <body>
        <h1>Données personnelles</h1>
        <form method="post" action="./deux.php"> 
            nom    : <input id="nomInput" type="text" name="nom" value="<?php echo $_SESSION['nom']; ?>" required placeholder="azerty" ?> <br/>
            prenom : <input id="prenomInput" type="text" name="prenom" value="<?php echo $_SESSION['prenom']; ?>" required placeholder="qwerty" ?>
            <br/><br/>
            <input type="submit" name="validerNP" value="Page suivante"/>
            <button class="b1" onClick="window.location.href='./fin.php'" name="destroySess" type="button">Annuler</button>
        </form> 
    </body>
</html>