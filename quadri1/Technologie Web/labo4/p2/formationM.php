<?php 

class Cours
{
    private $_nameCour;
    private $_cost;
    private $_dateCour;
    private $_infoCourse;

    public function __construct($nameCour, $cost, $dateCour, $infoCourse) {
        $this->_nameCour = $nameCour;
        $this->_cost = $cost;
        $this->_dateCour = $dateCour;
        $this->_infoCourse = $infoCourse;
    }

    public function getName(){
        return $this->_nameCour;
    }

    public function getCost(){
        return $this->_cost;
    }

    public function getDate(){
        $listDate = explode("/", $this->_dateCour);
        $date = "Du " . $listDate[0] . " au " . $listDate[1] . " " . $listDate[2] . " " . $listDate[3];
        return $date;
    }

    public function getInfo(){
        return $this->_infoCourse;
    }

    public function getType(){
        return strtolower($this->getName());
    }
}



function checkIfFormationIsAvailable($formation, $formationAvailable)
{
    $loacalFormation = 'noFormation';
    foreach ($formationAvailable as $formationAvailable)
    {
        if ($formationAvailable->getType() == $formation)
        {
            $loacalFormation = $formationAvailable;
        }
    }
    return $loacalFormation;
}
