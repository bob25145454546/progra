<?php 

class Panier
{
    private $_courses;

    public function __construct() {
        $this->_costOfArticles = 0;
        $this->_courses = array();
    }

    public function getCostOfArticles(){
        $costOfArticles = 0;
        foreach($this->_courses as &$course)
        {
            $costOfArticles += $course->getCost();
        }
        return $costOfArticles;
    }

    public function getAllCourses(){
        return $this->_courses;
    }

    public function addCourse($course){
        array_push($this->_courses, $course);
    }

    public function deleteAllCourse(){
        $this->_courses = array();
    }
}