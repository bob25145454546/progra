<?php 

require('controller.php');

if (session_status() == PHP_SESSION_NONE){
    session_start();
}

if(!isset($_SESSION['flag'])){
    initialisation();
}


if(isset($_POST['catalogueToDetailsFormation'])){
    detailsFormation();
}

elseif(isset($_POST['formationinscription'])){
    validerAchat();
}

elseif(isset($_POST['nextPageValidation']) || (isset($_POST['catalogueToPanier']))){
    displayPanier();
}

elseif(isset($_POST['viderPanier'])){
    clearPanier();
}

else{
    listFormation();
}


