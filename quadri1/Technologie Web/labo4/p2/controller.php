<?php 

require('formationM.php');
require('panierM.php');

function initialisation()
{
    $_SESSION['flag'] = 1;
    $xml = new Cours('XML',90,"07/13/décembre/2199","Le but est de comprendre les pincipales fonctionnalitées en xml.");
    $php = new Cours("PHP",120,"18/25/janvieer/2199","La finalité visée est d'acquérir un niveau avancé en php.");
    $ajax = new Cours("AJAX",50,"14/27/octobre/2199","L'objectif est d'apprendre les bases d'ajax.");
    $defaultCourse = new Cours('',0," ","");

    $listCoursesAvailable = array($xml,$php,$ajax);
    $_SESSION['listCoursesAvailable'] = $listCoursesAvailable;
    $_SESSION['defaultCourse'] = $defaultCourse;

    $panier = new Panier();
    $_SESSION['panier'] = $panier;
}


function listFormation()
{  
    $xml = checkIfFormationIsAvailable('xml',$_SESSION['listCoursesAvailable']);
    $php = checkIfFormationIsAvailable('php',$_SESSION['listCoursesAvailable']);
    $ajax = checkIfFormationIsAvailable('ajax',$_SESSION['listCoursesAvailable']);
    require('catalogueV.php');
}

function detailsFormation()
{
    if($_POST["radioChoice"] == 'xml'){
        $formation = checkIfFormationIsAvailable('xml',$_SESSION['listCoursesAvailable']);
    }
    elseif($_POST["radioChoice"] == 'php'){
        $formation = checkIfFormationIsAvailable('php',$_SESSION['listCoursesAvailable']);
    }
    elseif($_POST["radioChoice"] == 'ajax'){
        $formation = checkIfFormationIsAvailable('ajax',$_SESSION['listCoursesAvailable']);
    }

    $_SESSION['formationChosen'] = $formation;
    require('formationV.php');
}


function validerAchat()
{
    $formationsInPanier = $_SESSION['panier']->getAllCourses($_SESSION['formationChosen']);

    if(!in_array($_SESSION['formationChosen'], $formationsInPanier)){
        $_SESSION['panier']->addCourse($_SESSION['formationChosen']);
        require('validerAchat.php');
    }
    else {
        require('achatDejaAcheter.php');
    }
}

function displayPanier()
{
    $listFormation = $_SESSION['panier']->getAllCourses();
    require('panierV.php');
}


function clearPanier()
{
    $_SESSION['panier']->deleteAllCourse();
    $listFormation = $_SESSION['panier']->getAllCourses();
    require('panierV.php');
}