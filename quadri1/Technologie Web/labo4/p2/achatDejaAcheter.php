<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Refus</title>
        <link rel="stylesheet" href="info.css">
    </head>
    <body>
        <h1>Achat déja validée</h1>
        <div> 
            La formation 
            <?php 
                echo $_SESSION['formationChosen']->getType();
            ?>
            a déja été ajouté à votre panier.
            <br/><br/> 
        </div>
        <form method="post" action="index.php">
            <input class="submitInput" id="subConfirm" type="submit" name="nextPageNoValidation" value="Suivant"/>
        </form>
    </body>
</html>