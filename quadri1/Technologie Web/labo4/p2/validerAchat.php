<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Validation</title>
        <link rel="stylesheet" href="info.css">
    </head>
    <body>
        <h1>Achat validée</h1>
        <div> 
            La formation 
            <?php 
                echo $_SESSION['formationChosen']->getType();
            ?>
            a été ajouté à votre panier.
            <br/><br/> 
        </div>
        <form method="post" action="index.php">
            <input class="submitInput" id="subConfirm" type="submit" name="nextPageValidation" value="Suivant"/>
        </form>
    </body>
</html>