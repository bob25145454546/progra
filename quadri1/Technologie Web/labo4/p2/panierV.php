<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Panier utilisateur</title>
        <link rel="stylesheet" href="info.css">
    </head>
    <body>
        <h1>Panier</h1>
        <div> 
            <?php 
                foreach($listFormation as &$course)
                {
                    echo $course->getName() . " " . $course->getCost()  . " €" . "<br/>";
                }
            ?>
            <br/>
        </div>
        <div> 
            TOTAL : <?php echo $_SESSION['panier']->getCostOfArticles()  . " €" . "<br/>";
                    ?>
            <br/><br/>
        </div>
        <form method="post" action="index.php">
            <input class="submitInput" id="subPanier" type="submit" name="viderPanier" value="Vider le panier"/>
            <input class="submitInput" id="subPanier" type="submit" name="retourCatalogue" value="Retour au catalogue"/>
        </form>
    </body>
</html>