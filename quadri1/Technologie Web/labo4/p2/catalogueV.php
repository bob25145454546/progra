<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Page des formations</title>
        <link rel="stylesheet" href="info.css">
    </head>
    <body>
        <h1>Catalogue de formation</h1>
        <form method="post" action="index.php">
            <div >
                    <label> <?php echo $xml->getName() . " :"; ?> </label>
                    <input 
                        id="xmlInput"
                        type="radio" 
                        name="radioChoice" 
                        value="xml"
                        required
                    />
                    <br/><br/>

                    <label> <?php echo $php->getName() . " :"; ?> </label>
                        <input 
                            id="phpInput"
                            type="radio" 
                            name="radioChoice" 
                            value="php" 
                            required
                        />
                        <br/><br/>
                        
                    <label> <?php echo $ajax->getName() . " :"; ?> </label>
                    <input 
                        id="ajaxInput"
                        type="radio" 
                        name="radioChoice" 
                        value="ajax" 
                        required
                    />
                    <br/><br/>
            </div >
            <input class="submitInput" id="catalogueSubmit" type="submit" name="catalogueToDetailsFormation" value="Voir la formation"/>
        </form>
        <form method="post" action="index.php">
            <input class="submitInput" id="catalogueSubmit" type="submit" name="catalogueToPanier" value="Voir le panier"/>
        </form>
    </body>
</html>