<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Détails de la formation</title>
        <link rel="stylesheet" href="info.css">
    </head>
    <body>
        <h1>Info sur la formation en <?php echo $formation->getName(); ?></h1>
        <div> 
            Objectif : <br/>
            <?php echo $formation->getInfo(); ?><br/><br/>

            Coût :<br/>
            <?php echo $formation->getCost(); ?> € <br/><br/>

            Date :<br/>
            <?php echo $formation->getDate(); ?><br/><br/>
            <br/><br/>
        </div>
        <form method="post" action="index.php"> 
            <input class="submitInput" id="formationSubmit" type="submit" name="formationTocatalogue" value="Retour au catalogue"/>
            <input class="submitInput" id="formationSubmit" type="submit" name="formationinscription" value="S'inscrire à la formation"/>
        </form>
    </body>
</html>