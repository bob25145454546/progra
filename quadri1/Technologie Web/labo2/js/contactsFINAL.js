/* 
Activité : gestion des contacts
*/

let fin = 1;

let c0 = `0 quitter`;
let c1 = `1 list des contacts`;
let c2 = `2 ajouté contact`;
let c3 = `3 supprimé contact`;
let choix = 5;

function infoContact() {
    let nom = prompt("Entrez le nom :");        
    let prenom = prompt("Entrez le prénom :");
    let contact = new Contact(nom, prenom);
    return contact;
}
 

class Contact {
    constructor(nom, prenom)
    {
        this.nom = nom;
        this.prenom = prenom;
    }
}

class Annuaire {
    listeContact = [];

    constructor()
    {
        this.initialContact();
    }

    initialContact()
    {
        let prenom = `Carole`;
        let nom = `Lévisse`;
        let contact = new Contact(nom, prenom);
        this.addContact(contact);

        prenom = `Mélodie `;
        nom = `Nelsonne`;
        contact = new Contact(nom, prenom);
        this.addContact(contact);

        prenom = `a `;
        nom = `z`;
        contact = new Contact(nom, prenom);
        this.addContact(contact);
    }

    addContact(contact) {this.listeContact.push(contact);}

    delContact(contactToRemove)
    {
        let numContact = 0;
        let flagContactRemove = false;
        let flagCompareCont = false;

        for (numContact; numContact < this.nbContact(); numContact++)
        {
            flagCompareCont = this.compareContact(contactToRemove, this.listeContact[numContact]);
            
            if (flagCompareCont)
            {
                this.listeContact.splice(numContact,1);
                flagContactRemove = true;
            }
        }
        return flagContactRemove;
    }

    getAllContacts()
    {
        let allContacts = ``;
        let numContact = 0;
        let nom;
        let prenom;

        for (numContact; numContact < this.nbContact(); numContact++)
        {
            nom = this.listeContact[numContact].nom;
            prenom = this.listeContact[numContact].prenom;
            allContacts += numContact+` : `+nom+` (nom)  `
                                        +prenom+` (prénom) \n`;
        }
        console.log(`voici vos contacts \n \n`+allContacts);
        alert(`voici vos contacts \n \n`+allContacts);
    }

    compareContact(contact1, contact2)
    {
        let listeComparaisonNom = [];
        let listeComparaisonPrenom = [];
        let lenghtPrenomC1 = 0;
        let lenghtPrenomC2 = 0;
        let flag = false;

        listeComparaisonNom.push(String(contact1.nom));
        listeComparaisonNom.push(String(contact2.nom));
        listeComparaisonPrenom.push(String(contact1.prenom));
        listeComparaisonPrenom.push(String(contact2.prenom));

        if (listeComparaisonNom[0] == listeComparaisonNom[1])
        {
            flag = !(lenghtPrenomC1 < lenghtPrenomC2) && !(lenghtPrenomC1 > lenghtPrenomC2);
            if (flag) {return true;}
        }
    }   

    nbContact() {return this.listeContact.length;}
}

annuaire = new Annuaire();

while (choix != 0)
{
    console.log(c0+`\n`+c1+`\n`+c2+`\n`+c3+`\n\n`+"choississez entre 0-1-2-3 :");
    choix = prompt(c0+`\n`+c1+`\n`+c2+`\n`+c3+`\n\n`+"choississez entre 0-1-2-3 :");

    if (choix == 1) {annuaire.getAllContacts();}

    if (choix == 2)
    {
        let info = infoContact();
        annuaire.addContact(info);
        console.log(`le contact : `+ info.nom+` (nom) `+info.prenom+` (prénom) a été ajoutée`);
        alert(`le contact : `+info.nom+` (nom) `+info.prenom+` (prénom) a été ajoutée`);
    }

    if (choix == 3) 
    {
        let info = infoContact();
        let flag = annuaire.delContact(info);

        if (flag)
        {
            console.log(`le contact : `+info.nom+` (nom) `+info.prenom+` a été supprimé`);
            alert(`le contact : `+info.nom+` (nom) `+info.prenom+` (prénom) a été supprimé`);
        }
        else
        {
            console.log(`le contact : `+info.nom+` (nom) `+info.prenom+` (prénom) n'a pas été supprimé car introuvable`);
            alert(`le contact : `+info.nom+` (nom) `+info.prenom+` (prénom) n'a pas été supprimé car introuvable`);
        }
    }
}

