$(document).ready(function(){
    $('#switcher-large').click(function(){
        $('body').addClass('large');
        $('body').removeClass('narrow');

        $('#switcher-narrow').addClass('hidden');
        $('#switcher-default').addClass('hidden');
    });

    $('#switcher-narrow').click(function(){
        $('body').addClass('narrow');
        $('body').removeClass('large');

        $('#switcher-large').addClass('hidden');
        $('#switcher-default').addClass('hidden');
    });

    $('#switcher-default').click(function(){
        $('body').removeClass('large');
        $('body').removeClass('narrow');

        $('#switcher-large').addClass('hidden');
        $('#switcher-narrow').addClass('hidden');
    });
    
    $('.button').click(function(){
        $(this).addClass('selected');
    });

    $('#switcher div').mouseenter(function(){
        $(this).addClass('hover');
    });

    $('#switcher div').mouseout(function(){
        $(this).removeClass('hover'); // fond vert
        $(this).removeClass('selected');
    });

    $('.button').dblclick(function(){
        $('#switcher div').removeClass('hidden');
    });
});