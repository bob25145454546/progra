$(document).ready(function(){
    $(".hide").hide();
    $("h2[class='bold']").css({"font-style": "italic"});
    $("ul#selected-plays").css({"border": "groove"});
    $("ul#selected-plays").css({"display":"flex", "justify-content": "flex-start"});
    //$('ul#selected-plays li :eq(13)').append("<img src=./images/email.png width='14' height='14' />");
    $('ul#selected-plays [href="mailto:henryiv@king.co.uk"]').addClass('mailto');
    $('ul#selected-plays [href="hamlet.pdf"]').addClass('pdflink');
    //$('ul#selected-plays li :eq(8)').append("<img src=./images/pdf.png width='14' height='14' />");
    //$('ul#selected-plays [href="http://www.shakespeare.co.uk/henryv.htm"]').css({"border": "1px solid"});
    $('ul#selected-plays [href="http://www.shakespeare.co.uk/henryv.htm"]').addClass('henrylink')
    
    //$("tr:odd").css({"background-color": "lightgrey"});
    $("tr:odd").addClass('alt');
    $('<div />').appendTo('body').append('Copyright Shakespare').addClass('footer');
});
