function [matrixIsTautology] = isTautology(cubeList)
    matrixHasDontCare = hasDontCare(cubeList); 
    
    if matrixHasDontCare
        matrixIsTautology = 1;
        
    elseif isUnate(cubeList)
        matrixIsTautology = 0;
        
    elseif hasCubesComplementary(cubeList)
        matrixIsTautology = 1;
        
    else
        [positiveCofactor, negativeCofactor] = cofacterization(cubeList);   
        matrixIsTautology = isTautology(positiveCofactor) && isTautology(negativeCofactor);
    end
end


function [boolMatrixHasDontCare] = hasDontCare(matrix)
%La fonction retourne 1 si la matrice en entr�e a un all don't care cube, 0 sinon.
%   La matrice cr�er un vecteur de r�f�rence compos�e de 3 un nombre de
%  fois = � la longeur de la matrice d'entr�e. Elle parcourt les lignes de
%  la matrice d'entr�e une � une et la compare avec le
%  vecteur de r�f�rence, si les 2 lignes sont �gale alors la comparaison sera = 1 
%  et la matrice a alors un all don't care


    boolMatrixHasDontCare = 0;
    sizeMatrix = size(matrix);  % r�cup�re nb les ligne en posit. 1 et nb colonnes en 2
    numberOfLines = sizeMatrix(1);  % r�cup�re nb ligne en position 1
    numberOfRows = sizeMatrix(2); 
    referenceVector = ones(1,numberOfRows)*3; % cr�er un vector de r�f�rence compos� de 3 (en ligne) pour faire une comparaison     
    
    for lineNumber = 1:numberOfLines      % parcourt les lignes, contient le num�ro de la ligne actuel
        currentLine = matrix(lineNumber,:); % s�lectionne la ligne actuel, arg. 1 = ligne et arg. 2 = colonne, tt deux � s�lectionner
               
        if isequal(currentLine, referenceVector)  % on compare si la ligne actuel avec la ligne (vecteur) de r�f�rence
            boolMatrixHasDontCare = 1;
            break;
        end 
    end
end

function [boolMatrixIsUnate] = isUnate(matrix)
%La fonction retourne 1 si la matrice en entr�e est monoforme (unate), 0 sinon
%   La fonction regarde colonne par colonne si une variable apparait dans 2
%   polarit� (en cherchant un 1 ou un 2) et retient 1 si c'est le cas, ensuite elle
%   regarde si les deux bool�en (=1 / =2) sont � 1, si c'est le cas alors
%   la matrice est unate (monoforme) sinon non.

    boolMatrixIsUnate = 1;
    
   for currentlyRow = matrix   % prend les colonnes de la mtrice une par une 
       variableEqualOne = ismember(1,currentlyRow); % regarde dans la colonne si la variable apparait non ni�
       variableEqualTwo = ismember(2,currentlyRow); % regarde dans la colonne si la variable apparait ni�
       variableHasTwoPolarity = (variableEqualOne>0) && (variableEqualTwo>0); % v�rifie si la variable apparait 2x

       if variableHasTwoPolarity % si la variable apparait 2x (non ni� et ni�) alors la matrice est biforme
           boolMatrixIsUnate = 0;
           break;
       end
   end    
end

function [boolMatrixHasCubesComplementary] = hasCubesComplementary(matrix)
%La fonction retourne 1 si le cubelist d'entr�e � une variable compl�mentaire
%   La fonction parcourt les lignes � la recherche d'un cube qui ne
%   contiendrait que des don't care � l'exception d'une variable pouvant
%   appara�tre, ensuite si elle trouve un cube ne contenant qu'une
%   variable, alors elle cr�er un cube compl�mentaire (c�d dire un cube
%   identique mais avec la variable ayant son �tat invers�) et regarde si
%   ce cube compl�mentaire se trouve dans la matrice originelle.

    boolMatrixHasCubesComplementary = 0;
    
    sizeMatrix = size(matrix);  % r�cup�re nb les ligne en posit. 1 et nb colonnes en 2
    numberOfLines = sizeMatrix(1);  % r�cup�re nb ligne en position 1
    
    for lineNumber = 1:numberOfLines      % parcourt les lignes, contient le num�ro de la ligne actuel
        currentLine = matrix(lineNumber,:); % s�lectionne la ligne actuel, arg. 1 = ligne et arg. 2 = colonne, tt deux � s�lectionner
        numberOfVarInCurrentLine = sum(currentLine < 3); % compte le nombre de variable qui existe dans un cube (soit <3) 

        if numberOfVarInCurrentLine == 1 % si le nombre de variable dans le cube actuel est =1, alors ca veut dire qu'elle n'apparait qu'une fois
            vectorWhereVarIsTwoOrOne = (currentLine == 2 | currentLine == 1); % on cr�er un vecteur de 1 la ou se trouve le 1 ou le 2 et de 0 ailleurs
            indiceOfVariable = find(vectorWhereVarIsTwoOrOne); % on prend son indice pour savoir ou se trouve dans le cube la variable  =1 ou =2
            complemementaryLineOfCurrentLine = currentLine; % on copie le cube pour faire une comparaison plus tard
            
            if currentLine(indiceOfVariable) == 1 % v�rifie que la variable = 1 pour invers� son �tat et obtenir la ligne compl�mentaire
                complemementaryLineOfCurrentLine(indiceOfVariable) = 2; % on transforme le cube en cube compl�mentaire (compl�mentaire de la variable)
            else
                complemementaryLineOfCurrentLine(indiceOfVariable) = 1;% m�me chose sauf qu'on remplace le 2 par 1 ici contrairement au dessus
            end
           
            complemementaryLineIsInMatrix = ismember(complemementaryLineOfCurrentLine,matrix,'rows'); % on regarde si le cube compl�mentaire se trouve dans la matrice
                                                                                                                                            % le rows permet de renvoy� juste un 1 et pas un vecteur de 1
             if complemementaryLineIsInMatrix % si le cube compl�mentaire se trouve dans la matrice, alors on change le flag, la fonction � une variable compl�mentaire
                 boolMatrixHasCubesComplementary = 1;
                 break;
            end
        end
    end    
end

function [positivMatrix, negativMatrix] = cofacterization(matrix)
%La fonction prend une matrice et renvois un cofacteur + et -
% La fonction commence par r�cup�rer l'index le plus balanc� de la matrice,
% puis elle applique les simplifications et cr�er les cofacteur + et - 
% enfonction que l'index de la variable soit =1 ou 2

    indexMoreBalancedRow = indexMoreBalancedVar(matrix); % r�cup�re l'index le plus bif et balanc�
    
    sizeMatrix = size(matrix);  % r�cup�re nb les ligne en posit. 1 et nb colonnes en 2
    numberOfLines = sizeMatrix(1);  % r�cup�re nb ligne en position 1    
    numberOfRows = sizeMatrix(2); % r�cup�re nb colonne en position 2 
    
    lineNumberPos = 1; % valeur des lignes actuel de chaque matrices
    lineNumberNeg = 1;
    
    positivMatrix = 1:numberOfRows; % initialisation des matrices
    negativMatrix = 1:numberOfRows;
    
    for lineNumber = 1:numberOfLines      % parcourt les lignes, contient le num�ro de la ligne actuel                       
        currentLine = matrix(lineNumber,:); % s�lectionne la ligne actuel, arg. 1 = ligne et arg. 2 = colonne
        valueOfMoreBalancedVar = currentLine(indexMoreBalancedRow); % prend dans la ligne actuel la valeur de la variable la plus balanc� & biforme
        
        if valueOfMoreBalancedVar == 1   % v�rifie si la valeur de la colonne de la variable permettant la factorisation est =1 pour la matrice positive            
            currentLine(:, indexMoreBalancedRow) = 3; % remaplcement par 3 de l'index de la variable la matrice positive
            positivMatrix(lineNumberPos,:) = currentLine; % remplace la valeur � l'endroit ou il y a un 1 par 3
            lineNumberPos = lineNumberPos+1;
            
        elseif valueOfMoreBalancedVar == 2    % v�rifie si la valeur de la colonne de la variable permettant la factorisation est =2 pour la matrice positive
            currentLine(:, indexMoreBalancedRow) = 3;
            negativMatrix(lineNumberNeg,:) = currentLine;  % enl�ve la ligne de la matrice 
            lineNumberNeg = lineNumberNeg+1;
        else
            positivMatrix(lineNumberPos,:) = currentLine; % rquand c'est = 3, on place la ligne sans modif dans les matrices          
            negativMatrix(lineNumberNeg,:) = currentLine;  
            lineNumberPos = lineNumberPos+1;
            lineNumberNeg = lineNumberNeg+1;
        end  
    end       
end

function [indexMoreBalancedRow] = indexMoreBalancedVar(matrix)
%La fonction prend une matrice et renvois l'index de la variable la plus balanc�


    flag = 1; % permet de passer une premi�re fois dans le if pour mettre � jour la valeur du nbr d'occurence des variables    
    maximumNumberOccurence = 0; % nombre d'occurence maximum enregistr� des variables =1 et =2
    rowNumber = 0; % permet de savoir � quel colonne on est et donc conna�tre la variable recherch�

    for currentRow = matrix   % prend les colonnes de la matrice une par une 
        rowNumber = rowNumber+1;
        currentNumberOccurenceVar =  sum(currentRow == 2) + sum(currentRow == 1); % compte le nombre d'occurence de 1 et 2   

        if (maximumNumberOccurence < currentNumberOccurenceVar) || flag % permet de passer une 1� fois gr�ce au flag, puis repasse seulement 
            flag = 0;                                                                           % quand le nombre d'occurence est plus grand            
            maximumNumberOccurence = currentNumberOccurenceVar; % actualisation du maximum d'occurence de la variable                
            indexRowMoreBinate = rowNumber;   % r�cup�ration de la colonne ou se trouve la variable la plus biforme
            indexMoreBalancedRow = rowNumber; % r�cup�ration de la colonne ou se trouve la variable la plus balanc�
            
        elseif maximumNumberOccurence == currentNumberOccurenceVar % si la biformit� est =, alors on regarde la var a plus balanc�
            lastRowMoreBinate = matrix(:,indexRowMoreBinate); % on r�cup�re la derni�re variable la plus biforme
            balancementCurrentRow = abs(sum(currentRow == 2) - sum(currentRow == 1)); % on regarde le niveau de balancement
            balancementLastRow = abs(sum(lastRowMoreBinate == 2) - sum(lastRowMoreBinate == 1)); % idem pour la derni�re variable
            
            if balancementCurrentRow < balancementLastRow % on regarde entre les 2 variable celle qui est la plus balanc�
                indexMoreBalancedRow = rowNumber;   % r�cup�ration de la colonne ou se trouve la variable de s�paration en cofacteur
            else
                indexMoreBalancedRow = indexRowMoreBinate;   % r�cup�ration de la colonne ou se trouve la variable de s�paration en cofacteur
            end
        end
    end
end





