


void test(){
	
	int i = 12;
	int *p[2]; // array contenant des adresses d'ou le * 
	
	p[0] = &i; // place une adresse dans l'array d'addresse
	/*
	printf("p0 : %d \n",*p[0]);
	printf("p1 : %d \n",&p[1]);
	*/
	int data[] = {99,98,97};
	int (*q)[]; // pointeur vers un array
	
	q = &data; // on lui affecte l'adresse d'un array
	
	printf("q0 : %d \n",(*q)[0]);
	printf("q1 : %d \n",(*q)[1]);
	printf("q2 : %d \n",(*q)[2]);
	printf("q : %d \n",q);
	printf("data : %d \n",data);
	 
}