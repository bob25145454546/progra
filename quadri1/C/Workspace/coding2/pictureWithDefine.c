#include <stdio.h>

#define forES(d,f) for(d=0; d <= (f-1)/2; d++) printf(" ");
#define forET(d,f) for(d=0; d < f; d++) printf("*");

#define firstW "by"
#define lastW "CBF"

void pictureD()
{
	int nbLignes = 0;
	int debut = 0;
	int nbEtoile = 1;
	int nbEspace = 13;

	for (nbLignes = 0; nbLignes <= 9; ++nbLignes) // rajoute un espace si jusque 9, 8 si on veut le end directe
	{
		if (nbLignes == 8) printf("    %s ",firstW);
		
		else forES(debut, nbEspace)
		
		if (nbLignes == 4) printf(" Hello, World!");
		
		else forET(debut, nbEtoile) // ne rentre plus car fin < debut
		
		if (nbLignes == 8) printf(" %s",lastW);

        printf("\n");

        if (nbLignes < 4)
        {
            nbEtoile += 4;
            nbEspace -= 4;
        }
        if (nbLignes >= 4)
        {
            nbEtoile -= 4;
            nbEspace += 4;
        }
	}
}