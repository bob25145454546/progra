##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=coding2
ConfigurationName      :=Debug
WorkspaceConfiguration := $(ConfigurationName)
WorkspacePath          :=C:/Users/195286/Desktop/progra/C/Workspace
ProjectPath            :=C:/Users/195286/Desktop/progra/C/Workspace/coding2
IntermediateDirectory  :=../build-$(ConfigurationName)/coding2
OutDir                 :=../build-$(ConfigurationName)/coding2
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=195286
Date                   :=25/11/2019
CodeLitePath           :="C:/Program Files/CodeLite"
LinkerName             :=C:/TDM-GCC-64/bin/g++.exe
SharedObjectLinkerName :=C:/TDM-GCC-64/bin/g++.exe -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=..\build-$(ConfigurationName)\bin\$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :=$(IntermediateDirectory)/ObjectsList.txt
PCHCompileFlags        :=
RcCmpOptions           := 
RcCompilerName         :=C:/TDM-GCC-64/bin/windres.exe
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := C:/TDM-GCC-64/bin/ar.exe rcu
CXX      := C:/TDM-GCC-64/bin/g++.exe
CC       := C:/TDM-GCC-64/bin/gcc.exe
CXXFLAGS :=  -g -O0 -Wall $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := C:/TDM-GCC-64/bin/as.exe


##
## User defined environment variables
##
CodeLiteDir:=C:\Program Files\CodeLite
Objects0=../build-$(ConfigurationName)/coding2/main.c$(ObjectSuffix) ../build-$(ConfigurationName)/coding2/pictureWithDefine.c$(ObjectSuffix) ../build-$(ConfigurationName)/coding2/picture.c$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: MakeIntermediateDirs $(OutputFile)

$(OutputFile): ../build-$(ConfigurationName)/coding2/.d $(Objects) 
	@if not exist "..\build-$(ConfigurationName)\coding2" mkdir "..\build-$(ConfigurationName)\coding2"
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@if not exist "..\build-$(ConfigurationName)\coding2" mkdir "..\build-$(ConfigurationName)\coding2"
	@if not exist ""..\build-$(ConfigurationName)\bin"" mkdir ""..\build-$(ConfigurationName)\bin""

../build-$(ConfigurationName)/coding2/.d:
	@if not exist "..\build-$(ConfigurationName)\coding2" mkdir "..\build-$(ConfigurationName)\coding2"

PreBuild:


##
## Objects
##
../build-$(ConfigurationName)/coding2/main.c$(ObjectSuffix): main.c ../build-$(ConfigurationName)/coding2/main.c$(DependSuffix)
	$(CC) $(SourceSwitch) "C:/Users/195286/Desktop/progra/C/Workspace/coding2/main.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/coding2/main.c$(DependSuffix): main.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/coding2/main.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/coding2/main.c$(DependSuffix) -MM main.c

../build-$(ConfigurationName)/coding2/main.c$(PreprocessSuffix): main.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/coding2/main.c$(PreprocessSuffix) main.c

../build-$(ConfigurationName)/coding2/pictureWithDefine.c$(ObjectSuffix): pictureWithDefine.c ../build-$(ConfigurationName)/coding2/pictureWithDefine.c$(DependSuffix)
	$(CC) $(SourceSwitch) "C:/Users/195286/Desktop/progra/C/Workspace/coding2/pictureWithDefine.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/pictureWithDefine.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/coding2/pictureWithDefine.c$(DependSuffix): pictureWithDefine.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/coding2/pictureWithDefine.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/coding2/pictureWithDefine.c$(DependSuffix) -MM pictureWithDefine.c

../build-$(ConfigurationName)/coding2/pictureWithDefine.c$(PreprocessSuffix): pictureWithDefine.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/coding2/pictureWithDefine.c$(PreprocessSuffix) pictureWithDefine.c

../build-$(ConfigurationName)/coding2/picture.c$(ObjectSuffix): picture.c ../build-$(ConfigurationName)/coding2/picture.c$(DependSuffix)
	$(CC) $(SourceSwitch) "C:/Users/195286/Desktop/progra/C/Workspace/coding2/picture.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/picture.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/coding2/picture.c$(DependSuffix): picture.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/coding2/picture.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/coding2/picture.c$(DependSuffix) -MM picture.c

../build-$(ConfigurationName)/coding2/picture.c$(PreprocessSuffix): picture.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/coding2/picture.c$(PreprocessSuffix) picture.c


-include ../build-$(ConfigurationName)/coding2//*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(IntermediateDirectory)


