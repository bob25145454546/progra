#include <stdio.h>

#define firstW "by"
#define lastW "CBF"

void picture()
{
	int nbLignes = 0;
	int debut = 0;
	int nbEtoile = 1;
	int nbEspace = 13;

	for (nbLignes = 0; nbLignes <= 9; ++nbLignes) // rajoute un espace si jusque 9, 8 si on veut le end directe
	{
		if (nbLignes == 8) printf("    %s ",firstW);
		
		else for (debut = 0; debut <= (nbEspace-1)/2; debut++) printf(" ");
		
		if (nbLignes == 4) printf(" Hello, World!");
		
		else for (debut = 0; debut < nbEtoile; debut++) printf("*"); // ne rentre plus car fin < debut
		
		if (nbLignes == 8) printf(" %s",lastW);

        printf("\n");

        if (nbLignes < 4)
        {
            nbEtoile += 4;
            nbEspace -= 4;
        }
        if (nbLignes >= 4)
        {
            nbEtoile -= 4;
            nbEspace += 4;
        }
	}
}