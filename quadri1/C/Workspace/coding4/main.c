#include <stdio.h>
#include <stdlib.h>
//#include "analyseFunction.c"

int *analyzeArray(int*,int);

int main(int argc, char **argv)
{

	int arrayIn[] = {0,10,5,8,2,4};
	int tailleArrayIn = sizeof(arrayIn)/sizeof(arrayIn[0]);
	int *arrayOut;
	
	arrayOut = analyzeArray(arrayIn, tailleArrayIn);

	if(arrayOut == NULL){
		printf("error");
		return 0;
	}

	printf(" max : %d \n",arrayOut[0]);
	printf(" min : %d \n",arrayOut[1]);
	printf(" moy : %d \n",arrayOut[2]);
	printf(" lastval : %d \n",arrayOut[3]);
	
	printf("\n");
	
	free(arrayOut);

	return 0;
}




