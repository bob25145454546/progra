/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.77
        Device            :  PIC18F46K22
        Driver Version    :  2.00
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#include "mcc_generated_files/mcc.h"

/*
                         Main application
 */

adc_result_t adr;

void main(void)
{
    // Initialize the device
    SYSTEM_Initialize();

    // If using interrupts in PIC18 High/Low Priority Mode you need to enable the Global High and Low Interrupts
    // If using interrupts in PIC Mid-Range Compatibility Mode you need to enable the Global and Peripheral Interrupts
    // Use the following macros to:

    // Enable the Global Interrupts
    INTERRUPT_GlobalInterruptEnable();// INT0 EST PAS PERIPHERIQUE

    // Disable the Global Interrupts
    //INTERRUPT_GlobalInterruptDisable();

    // Enable the Peripheral Interrupts
    //INTERRUPT_PeripheralInterruptEnable();

    // Disable the Peripheral Interrupts
    //INTERRUPT_PeripheralInterruptDisable();
    
    
    
    uint16_t dutyValue;//dclaration de la variable contenant le duty (sur 1023)
    
    while (1)
    {
      //---------------------------------------   INTERRUPTIONS  
       /*
        voir code qui sera excuter par l'interruption dans ext_int 
        * puis dans  void INT0_ISR(void),
        * dans notre cas on allume tt les leds, le boutons pousosir
        * n'est pas configurer ici mais est cabler physiquement sur 
        * la carte
        */
        
        //---------------------------------------------- PWM
        /*
        adr = ADC_GetConversion(0);
        dutyValue = adr; // rgle le pwm en fonctin de l'adc
        PWM4_LoadDutyValue(dutyValue); // appel de la fct qui set le duty cycle
        */
        
        
        //----------------------------------------------  ADC
        /*
        adc_channel_t chann = 0;
        adr = ADC_GetConversion(chann);
        
        if (adr >= 255){
            LATAbits.LATA4 = 1;
        }
        if (adr >= 500){
            LATAbits.LATA5 = 1;
        }
        if (adr >= 755){
            LATAbits.LATA6 = 1;
        }
        if (adr >= 1023){
            LATAbits.LATA7 = 1;
        }
        else{
            // les lampes vont s'teindre et se rallumer trs vite, du cou^p
            // elle brilleront plus vite quand tt  1 (car le else pas valide)
            // et brilleonrs moins quand pas tt allum vus que elles s'teignent
            LATAbits.LATA4 = 0;
            LATAbits.LATA5 = 0;
            LATAbits.LATA6 = 0;
            LATAbits.LATA7 = 0;
        }
        */
        
        
        //----------------------------------------------- DELAY + BP + LED
        /*
        LATAbits.LATA4 = 1;
        __delay_ms(1000);
        LATAbits.LATA4 = 0;
        __delay_ms(1000);
        
        if(PORTBbits.RB4){      // lit ce qu il y a sur le port RB4 (BP) 
            LATAbits.LATA4 = 1; // ecrit un 1 sur le port A4
        }
        else{
            LATAbits.LATA4 = 0;
        }
        LATAbits.LATA5 = 1;
         */
    }
}

/**
 End of File
*/